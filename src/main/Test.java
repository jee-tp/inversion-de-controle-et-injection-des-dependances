package main;

import dao.DaoImp;
import dao.Idao;
import metier.Imetier;
import metier.MetierImp;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Scanner;

public class Test {

    public static void main(String[] args) {
        //Instanciation statique

       /* DaoImp dao=new DaoImp();
        MetierImp metier=new MetierImp();

        metier.setIdao(dao);
        System.out.println(metier.calcul());*/

        //-----------instanciation dynamique----------------

        /*try {

            Scanner scanner =new Scanner(new File("config.txt"));
            String daoClassName=scanner.next();
            String metierClassName=scanner.next();
            Class daoImp=Class.forName(daoClassName);
            Class metierImp=Class.forName(metierClassName);

            Idao dao= (Idao) daoImp.newInstance();
            Imetier metier = (Imetier) metierImp.newInstance();
            Method methodSetDao =metier.getClass().getMethod("setIdao",Idao.class);
            methodSetDao.invoke(metier,dao);
            System.out.println(metier.calcul());
        }
        catch (Exception e){e.printStackTrace();}

    */


        //--------------Injection des dépendances avec Spring.---------------


        //En utilisant les annotations spring
       // ApplicationContext context =new AnnotationConfigApplicationContext("dao","metier");
        //Utilisant le fichier Xml
        ApplicationContext context=new
                ClassPathXmlApplicationContext("spring-config.xml");
        Imetier metier=(Imetier) context.getBean("metier");
       //Imetier  imetier = context.getBean(Imetier.class);
       System.out.println(metier.calcul());
       //testnj
    }
}
