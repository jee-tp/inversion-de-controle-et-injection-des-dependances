package metier;

import dao.Idao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MetierImp implements  Imetier{



    @Autowired
    private Idao idao;
    @Override
    public double calcul() {
        double result=idao.getValue();
        return Math.pow( result,Math.PI);
    }

    public void setIdao(Idao idao) {
        this.idao = idao;
    }
}
